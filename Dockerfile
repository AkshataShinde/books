FROM golang:1.19
ENV GO111MODULE=on
ENV GOPROXY=https://proxy.golang.org

RUN mkdir -p /app
ADD . /app
WORKDIR /app

COPY go.mod /usr/src/app


RUN go mod download

COPY . /app
ENV GOVCS=*:off
# Build the application
RUN go build -o main .


# Export necessary port
EXPOSE 8080

# Command to run when starting the container
CMD ["/app/main"]
