package main

import (
	//"net/http"

	//_ "books/docs"
	"books/models"
	"books/routes"

	"github.com/gin-gonic/gin"
	//jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/joho/godotenv"
)

// @title Book API With JWT
// @version 1.0
// @Description Golang basic API with JWT Authentication and Authorization.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:5432
// @BasePath /
func main() {
	// r := gin.Default()
	godotenv.Load()          // Load env variables
	models.ConnectDataBase() // load db
	var router = make(chan *gin.Engine)
	go routes.GetRouter(router)
	r := <-router
	r.Run()
}
